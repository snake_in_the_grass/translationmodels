# Natural Language Processin toolkit
from nltk.translate.bleu_score import SmoothingFunction, sentence_bleu, corpus_bleu

from data_loader import load_dataset, max_length, convert, preprocess_sentence
# from model import Encoder, BidirectionalEncoder, ReverseEncoder, Decoder
from model import Encoder, Decoder

import pickle
import os
import argparse

import numpy as np
import tensorflow as tf

def initialise_lite(language_choice):
    """Initialise Lite
    
    Initialises the input and targer vocabulary as well as the encoder and decoder.
    This is used for inference purposes but could also be used for training if you 
    want to use the same dataset and langauge models.
    """
    global units
    BATCH_SIZE = 64
    embedding_dim = 256
    units = 1024
    optimizer = tf.keras.optimizers.Adam()

    # Read in data and dictionary
    data_dir = './dictionaries/' + language_choice + '/'
    
    print('Unpickling the language models from {}'.format(data_dir))
    global inp_lang, targ_lang
    with open( data_dir + 'inp_lang.dump', 'rb') as file:
        inp_lang = pickle.load(file)

    with open( data_dir + 'targ_lang.dump', 'rb') as file:
        targ_lang = pickle.load(file)
    
    vocab_inp_size = len(inp_lang.word_index) + 1
    vocab_tar_size = len(targ_lang.word_index) + 1

    print('Input vocab size: ', vocab_inp_size)
    print('Target vocab size: ', vocab_tar_size)

    global max_length_inp, max_length_targ
    
    max_length_inp, max_length_targ = 25 , 25

    # Init encoder and decoder
    global encoder, decoder 
    # if(language_choice == 'ge_15002'):
    #     print('Forward encoder.')
    #     encoder = Encoder(vocab_inp_size, embedding_dim, units, BATCH_SIZE, language_choice)
    # elif(language_choice == 'ge_15002_reverse'):
    #     print('Reverse encoder.')
    #     encoder = ReverseEncoder(vocab_inp_size, embedding_dim, units, BATCH_SIZE)
    # elif(language_choice == 'ge_15002_bidirectional'):
    #     print('Bidirectional encoder.')
    #     encoder = BidirectionalEncoder(vocab_inp_size, embedding_dim, units, BATCH_SIZE)
    # else:
    #     print('Default encoder.')
        # encoder = Encoder(vocab_inp_size, embedding_dim, units, BATCH_SIZE, language_choice)

    encoder = Encoder(vocab_inp_size, embedding_dim, units, BATCH_SIZE)
    decoder = Decoder(vocab_tar_size, embedding_dim, units, BATCH_SIZE)    

    # Init checkpoint object 
    global checkpoint, checkpoint_dir, checkpoint_prefix, manager
    
    checkpoint_dir = "./checkpoints/"+language_choice
    print('Checkpoint directory: {}'.format(checkpoint_dir))
    checkpoint_prefix = os.path.join(checkpoint_dir, "ckpt")
    checkpoint = tf.train.Checkpoint(
        optimizer=optimizer, encoder=encoder, decoder=decoder)
    if(tf.train.latest_checkpoint(checkpoint_dir)):
        print(tf.train.latest_checkpoint(checkpoint_dir))
        print(checkpoint.restore(tf.train.latest_checkpoint(checkpoint_dir)))
    else:
        print("I think it couldnt find the checkpoint.")
        exit()

def read_test_data(language_choice):
    path_to_files = './test_data/' + language_choice + '/'

    if(language_choice == 'ge'):
        lines_inp = open(path_to_files + 'newstest2015.en', encoding="UTF-8").read().strip().split("\n")
        lines_targ = open(path_to_files + 'newstest2015.ge', encoding="UTF-8").read().strip().split("\n")
    elif(language_choice == 'dgs'):
        lines_inp = open(path_to_files + 'pheonix2014.de', encoding="UTF-8").read().strip().split("\n")
        lines_targ = open(path_to_files + 'pheonix2014.dgs', encoding="UTF-8").read().strip().split("\n")        

    sentence_pairs = [[preprocess_sentence(inp), preprocess_sentence(targ)] for inp, targ in zip(lines_inp, lines_targ) ]

    return sentence_pairs

def evaluate(sentence, language_choice, include_unk=True):
    """Process unicode string sentence as input to network
    
    Returns an empty string and zero initialised attention plot if there 
    is an out of vocabulary word encountered.
    """
    # Clean the sentence (remove some characters and split the punctuation)
    # sentence = preprocess_sentence(sentence)

    # Split the sentence up into words and then map each word to its word-id

    inputs = []
    for word in sentence.split(" "):
        try:
            inputs.append(inp_lang.word_index[word])
        except KeyError:
            # print('[evaluation] Unknown word, {}, replacing with <unk>'.format(word))
            if(not include_unk):
                return '', sentence
            
            # print('BOOOOOOOO')
            if(language_choice == 'ge_15002'):
                inputs.append(inp_lang.word_index['<unk>'])
            elif(language_choice == 'ge_15002_bidirectional' or language_choice == 'ge_15002_reverse'):
                inputs.append(inp_lang.word_index['unk'])
            elif(language_choice == 'dgs'):
                inputs.append(inp_lang.word_index['unk'])
            

    inputs = tf.keras.preprocessing.sequence.pad_sequences(
        [inputs], maxlen=max_length_inp, padding="post"
    )
    inputs = tf.convert_to_tensor(inputs)

    result = "<start> "

    hidden = [tf.zeros((1, units))]

    # Pass input to encoder
    enc_out, enc_hidden = encoder(inputs, hidden)

    # Initialise decoder hidden state with the encoder's and start
    dec_hidden = enc_hidden

    # Add a batch dimension to a single element
    dec_input = tf.expand_dims([targ_lang.word_index["<start>"]], 0)

    for t in range(max_length_targ):
        predictions, dec_hidden, attention_weights = decoder(
            dec_input, dec_hidden, enc_out
        )

        # storing the attention weights to plot later on
        attention_weights = tf.reshape(attention_weights, (-1,))

        # Grab the most likely prediction
        predicted_id = tf.argmax(predictions[0]).numpy()

        # Convert id to word and add to the result sentence
        result += targ_lang.index_word[predicted_id] + " "

        if targ_lang.index_word[predicted_id] == "<end>":
            return result, sentence

        # the predicted ID is fed back into the model
        dec_input = tf.expand_dims([predicted_id], 0)

    return result, sentence

def reference_hypothesis(sentence_pairs, language_choice, ink_unk=True):
    list_ref =[]
    list_hyp = []
    length = 0
    if(ink_unk):
        flag = '_inc_unk_'
    else:
        flag = '_no_unk_'
    with open('translations' + language_choice + flag + '.table', 'w') as table:

        for i, (inp, targ) in enumerate(sentence_pairs):

            prediction, _inp = evaluate(inp, language_choice, ink_unk)  # prediction should return empty string if unk encountered and ink_unk = False

            if(prediction):
                list_ref.append([targ])
                list_hyp.append(prediction)
                length += 1

                if(length <  10):
                    inp = ' '.join(inp.strip().split(' ')[1:-1])
                    targ = ' '.join(targ.strip().split(' ')[1:-1])
                    prediction = ' '.join(prediction.strip().split(' ')[1:-1])

                    print('\hline')
                    print('Input &', inp, '\\\ ')
                    print('Ground Truth & ', targ, '\\\ ')
                    print('Translation & ', prediction, '\\\ ')
                    table.write('\hline\n')
                    table.write('Input & {} \\\ \n'.format(inp))
                    table.write('Ground Truth & {} \\\ \n'.format(targ.upper()))
                    table.write('Translation & {} \\\ \n'.format(prediction.upper()))
            else:
                pass
                # print('Not including due to <unk> encountered.')

    return list_ref, list_hyp


# ------------------------------------------------------------------------
# Main Method
# ------------------------------------------------------------------------
if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--lang', help="required to select language from: \
                        german (ge), afrikaans (af), DGS (dgs), SASL (sasl)")

    # Collect arguments
    args = parser.parse_args()
    language_choice = args.lang
    print('----------------------------------------')
    print('Language choice: ', language_choice)
    print('----------------------------------------')

    # Load the model
    initialise_lite(language_choice)
    # Reads the file based on the first part of the string 'language_choice' therefore ge_15000 -> ge
    sentence_pairs = read_test_data(language_choice.split('_')[0])

    # Sentences with no OOV words only
    print('\n1. Without OOV words:')
    list_ref, list_hyp = reference_hypothesis(sentence_pairs, language_choice, False)
    cb = corpus_bleu(list_ref, list_hyp)
    print('Number of sentences compared:' ,len(list_ref))
    print(cb, '\n')


    # Sentences with OOV words
    if(language_choice != 'ge' or language_choice != 'dgs'):
        print('2. Including OOV words:')
        list_ref, list_hyp = reference_hypothesis(sentence_pairs, language_choice)
        cb = corpus_bleu(list_ref, list_hyp)
        print('Number of sentences compared:' ,len(list_ref))
        print(cb)