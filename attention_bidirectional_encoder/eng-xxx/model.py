# ------------------------------------------------------------------------
# Author:       Jake Pencharz
# Description:  Methods and classes used for an encoder-decoder model
# Date:         July 2019
# Project:      Translate English to German, UCT Final year project
# 
# ** Based on tensorflow tutorial: 
# https://www.tensorflow.org/beta/tutorials/text/nmt_with_attention
# ------------------------------------------------------------------------

from __future__ import absolute_import, division, print_function
import tensorflow as tf
# tf.enable_eager_execution()
tf.compat.v1.enable_eager_execution()
import numpy as np

'''
Encoder Class
'''
class Encoder(tf.keras.Model):
    def __init__(self, vocab_size, embedding_dim, enc_units, batch_sz):
        super(Encoder, self).__init__()
        self.batch_sz = batch_sz
        self.enc_units = enc_units

        # LAYER 1: Word Embedding layer     
        self.embedding = tf.keras.layers.Embedding(vocab_size, embedding_dim)
        
        # LAYER 2: Bidirectional layer
        self.gru_forward = tf.keras.layers.GRU(self.enc_units,
                                    return_sequences=True,  # returns output at each step
                                    return_state=True,
                                    recurrent_initializer='glorot_uniform')
        self.gru_backward = tf.keras.layers.GRU(self.enc_units,
                                    return_sequences=True,  # returns output at each step
                                    return_state=True,
                                    go_backwards = True,
                                    recurrent_initializer='glorot_uniform')
        # self.bidirectional = tf.keras.layers.Bidirectional(gru, merge_mode='concat')
        
        # LAYER 3: GRU
        self.gru_2 = tf.keras.layers.GRU(self.enc_units,
                                    return_sequences=True,  # returns output at each step
                                    return_state=True,
                                    recurrent_initializer='glorot_uniform')

    # Note: The GRU processes a batch at a time, therefore it returns a 
    # 3 dimensional output tensor (batch_size, sequence length, units)
    # which captures the hidden state of the GRU at each timestep, 
    # for every sentence in the batch
    def call(self, x, hidden):
        '''
        Parameters:
        - hidden: list for the inital_states of the GRUs [(batch_size, units),(batch_size, units)]
        Returns:
        - output: 
        '''
        x = self.embedding(x)
        output_forward, hidden_forward = self.gru_forward(x, initial_state=hidden)
        output_backward, hidden_backward = self.gru_backward(x, initial_state=hidden)
        output_bi = tf.concat([output_forward, output_backward], axis=-1)
        output , hidden = self.gru_2(output_bi, initial_state=hidden)

        return output, hidden

    def initialize_hidden_state(self):
        return tf.zeros((self.batch_sz, self.enc_units))

'''
Attention Class
'''
class BahdanauAttention(tf.keras.Model):
    def __init__(self, units):
        super(BahdanauAttention, self).__init__()
        self.W1 = tf.keras.layers.Dense(units)
        self.W2 = tf.keras.layers.Dense(units)
        self.V = tf.keras.layers.Dense(1)

    def call(self, query, values):
        # hidden shape == (batch_size, hidden size)
        # hidden_with_time_axis shape == (batch_size, 1, hidden size)
        # we are doing this to perform addition to calculate the score
        hidden_with_time_axis = tf.expand_dims(query, 1)

        # score shape == (batch_size, max_length, 1)
        # we get 1 at the last axis because we are applying score to self.V
        # the shape of the tensor before applying self.V is (batch_size, max_length, units)
        score = self.V(tf.nn.tanh(self.W1(values) + self.W2(hidden_with_time_axis)))

        # attention_weights shape == (batch_size, max_length, 1)
        attention_weights = tf.nn.softmax(score, axis=1)

        # context_vector shape after sum == (batch_size, hidden_size)
        context_vector = attention_weights * values
        context_vector = tf.reduce_sum(context_vector, axis=1)

        return context_vector, attention_weights

'''
Decoder Class
'''
class Decoder(tf.keras.Model):
    def __init__(self, vocab_size, embedding_dim, dec_units, batch_sz):
        super(Decoder, self).__init__()
        self.batch_sz = batch_sz
        self.dec_units = dec_units
        self.embedding = tf.keras.layers.Embedding(vocab_size, embedding_dim)
        self.gru1 = tf.keras.layers.GRU(self.dec_units,
                                   return_sequences=True,
                                   return_state=True,
                                   recurrent_initializer='glorot_uniform')
        # self.gru2 = tf.keras.layers.GRU(self.dec_units,
        #                            return_sequences=True,
        #                            return_state=True,
        #                            recurrent_initializer='glorot_uniform')
        self.fc = tf.keras.layers.Dense(vocab_size)

        # used for attention
        self.attention = BahdanauAttention(self.dec_units)

    def call(self, x, hidden_l1, enc_output):
        '''
        1. Uses attention mechanism to find context vector.
        2. Concatenates context vector with the input word
        3. Feeds concatenated input to first GRU layer
        4. Output of first GRU layer to second GRU layer
        5. Output from second layer to dense layer of size (vocab_size)

        parameters:
        x - input word (either <start> or the maximally likely prediction)
        hidden_l1 - hidden layer for the first GRU layer. This is used for attention
        hidden_l2 - hidden layer for the second GRU layer
        enc_output - this does not change afer the encoder has done its work. Used for attention

        returns:
        x - the predictions of the fully connected layer (length of the target vocabulary)
        state_l1 - updated hidden state for the first GRU layer
        state_l2 - updated hidden state for the second GRU layer
        attention_weights - the attention weights showing where in the enc_output the decoder focused during this time step
        '''

        # enc_output shape == (batch_size, max_length, hidden_size)
        context_vector, attention_weights = self.attention(hidden_l1, enc_output)

        # x shape after passing through embedding == (batch_size, 1, embedding_dim)
        # x is a sequence of length = 1 (one word)
        x = self.embedding(x)

        # x shape after concatenation == (batch_size, 1, embedding_dim + hidden_size)
        x = tf.concat([tf.expand_dims(context_vector, 1), x], axis=-1)

        # passing the concatenated vector to the GRU1
        # output shape (batch_size, 1, hidden_size)
        output, state_l1 = self.gru1(x)
        
        # passing output from gru1 to gru2
        # output, state_l2 = self.gru2(output, hidden_l2)

        # output shape == (batch_size * 1, hidden_size)
        output = tf.reshape(output, (-1, output.shape[2]))

        # output shape == (batch_size, vocab)
        predictions = self.fc(output)

        return predictions, state_l1, attention_weights

