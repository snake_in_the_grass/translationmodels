#!/bin/bash

# USAGE:
# ./compress <dir_name>

echo Tarring "$1"
tar -zcvf $1.tar.gz -X .excludes ./$1
# tar -zcvf pncjak001_Lab8.tar.gz  --exclude="*.DS_Store" --exclude="*.git/" --exclude="*.vscode" --exclude="*.o" --exclude="./Lab8_OOP/tut" ./Lab8_OOP
#tar -zcvf pncjak001_Lab8.tar.gz --exclude="./Lab8_OOP/.DS_Store" --exclude="./Lab8_OOP/.git/" --exclude="./Lab8_OOP/.vscode" --exclude="./Lab8_OOP/*.o" --exclude="./Lab8_OOP/tut" ./Lab8_OOP

