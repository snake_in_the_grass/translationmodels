import io
import re
import unicodedata
import tensorflow as tf

# ------------------------------------------------------------------------
# Dataset creation functions
# ------------------------------------------------------------------------

# Converts the unicode file to ascii
def unicode_to_ascii(s):
    return "".join(
        c for c in unicodedata.normalize("NFD", s) if unicodedata.category(c) != "Mn"
    )


def preprocess_sentence(w):
    w = unicode_to_ascii(w.lower().strip())

    # creating a space between a word and the punctuation following it
    # eg: "he is a boy." => "he is a boy ."
    w = re.sub(r"([?.!,¿])", r" \1 ", w)
    w = re.sub(r'[" "]+', " ", w)

    # replacing everything with space except (a-z, A-Z, ".", "?", "!", ",")
    w = re.sub(r"[^a-zA-ZäöüÄÖÜß?.!,]+", " ", w)

    w = w.rstrip().strip()

    # adding a start and an end token to the sentence
    # so that the model know when to start and stop predicting.
    w = "<start> " + w + " <end>"
    return w


# 1. Remove the accents
# 2. Clean the sentences
# 3. Return sentence pairs in the format: [ENGLISH, SPANISH]
def create_dataset(path, num_examples):
    # read lines from file
    lines = io.open(path, encoding="UTF-8").read().strip().split("\n")

    # generate cleaned setence pairs
    sentence_pairs = [
        [preprocess_sentence(w) for w in l.split("\t")] for l in lines[:num_examples]
    ]

    return zip(*sentence_pairs)


# Input parameters:
#     - lang: this is an array of sentences from a single language
# returns:
#     - tensor: this is the set of those same sentences but vectorised (each word is now represetned by its unique id)
#     - lang_tokenizer:
def tokenize(lang):
    lang_tokenizer = tf.keras.preprocessing.text.Tokenizer(filters="")

    # Creates an internal vocabulary for lang
    lang_tokenizer.fit_on_texts(lang)

    # Uses the tokeniser to vectorise the entire input language
    tensor = lang_tokenizer.texts_to_sequences(lang)

    # Pads all the sequences in the tensor to the same length (max sentence length)
    tensor = tf.keras.preprocessing.sequence.pad_sequences(tensor, padding="post")

    return tensor, lang_tokenizer


def load_dataset(path, num_examples=None):
    # creating cleaned input, output sentence pairs
    # input : English
    # target : German

    # get sentence pairs
    inp_lang, targ_lang = create_dataset(path, num_examples)

    # tokenise/vectorise sentence pairs
    input_tensor, inp_lang_tokenizer = tokenize(inp_lang)
    target_tensor, targ_lang_tokenizer = tokenize(targ_lang)

    return input_tensor, target_tensor, inp_lang_tokenizer, targ_lang_tokenizer

def max_length(tensor):
    return max(len(t) for t in tensor)

def convert(lang, tensor):
  for unique_id in tensor:
    if unique_id!=0:
      # method to go from index to word
      print ("%d \t----> %s" % (unique_id, lang.index_word[unique_id]))    