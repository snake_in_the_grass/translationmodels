# ------------------------------------------------------------------------
# Author:       Jake Pencharz
# Description:  Methods and classes used for an encoder-decoder model
# Date:         July 2019
# Project:      Translate English to German, UCT Final year project
#
# ** Based on tensorflow tutorial:
# https://www.tensorflow.org/beta/tutorials/text/nmt_with_attention
# Original file is located at
#     https://colab.research.google.com/drive/1vB4Pp7-SmdOmFy6umqgwGE5QZ-ozslkN
#
# @title Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ------------------------------------------------------------------------

"""# Neural Machine Translation with Attention"""

from __future__ import absolute_import, division, print_function, unicode_literals
from data_loader import load_dataset, max_length, convert, preprocess_sentence
from model import Encoder, Decoder, BahdanauAttention
from nltk.translate.bleu_score import SmoothingFunction, sentence_bleu, corpus_bleu
import time
import io
import os
import numpy as np
import re
import unicodedata
from sklearn.model_selection import train_test_split
import matplotlib.ticker as ticker
import matplotlib.pyplot as plt
import tensorflow as tf
import csv
import logging
logging.basicConfig(filename='eng_german_50epoch_150000.log',level=logging.DEBUG)


# tf.enable_eager_execution()
# tf.compat.v1.enable_eager_execution()


# ------------------------------------------------------------------------
# Training functions
# ------------------------------------------------------------------------
def loss_function(real, pred):
    mask = tf.math.logical_not(tf.math.equal(real, 0))
    loss_ = loss_object(real, pred)

    mask = tf.cast(mask, dtype=loss_.dtype)
    loss_ *= mask

    return tf.reduce_mean(loss_)


@tf.function
def train_step(inp, targ, enc_hidden):
    loss = 0

    with tf.GradientTape() as tape:
        # Encoder output shape: (batch size, sequence length, units) (64, 14, 1024)
        enc_output, enc_hidden = encoder(inp, enc_hidden)

        # Encoder Hidden state shape: (batch size, units) (64, 1024)
        dec_hidden = enc_hidden

        # Create a <start> for each of the sentences in the batch
        # Decoder output shape: (batch_size, vocab size) (64, 16912)
        dec_input = tf.expand_dims(
            [targ_lang.word_index['<start>']] * BATCH_SIZE, 1)

        for t in range(1, targ.shape[1]):
            # Passing enc_output to the decoder for the attention mechanism
            # and processing an entire batch at once
            predictions, dec_hidden, _ = decoder(
                dec_input, dec_hidden, enc_output)

            loss += loss_function(targ[:, t], predictions)

            # Teacher forcing - feeding the target as the next input
            # Note: ignoring the predictions generated due to teacher forcing
            # Note: that targ is a batch of setences. Therefore take the correct word
            # for each sentence in the batch at this timestep
            # and feed it to the next timestep.
            dec_input = tf.expand_dims(targ[:, t], 1)

    batch_loss = (loss / int(targ.shape[1]))

    variables = encoder.trainable_variables + decoder.trainable_variables

    gradients = tape.gradient(loss, variables)

    optimizer.apply_gradients(zip(gradients, variables))

    return batch_loss


@tf.function
def validation_step(inp, targ, enc_hidden):
    loss = 0

    # Encoder output shape: (batch size, sequence length, units) (64, 14, 1024)
    enc_output, enc_hidden = encoder(inp, enc_hidden)

    # Encoder Hidden state shape: (batch size, units) (64, 1024)
    dec_hidden = enc_hidden

    # Insert the index for <start> token for every sentences in the batch
    # Decoder output shape: (batch_size, vocab size) (64, 16912)
    dec_input = tf.expand_dims([targ_lang.word_index['<start>']] * BATCH_SIZE, 1)

    for t in range(1, targ.shape[1]):
        # Passing enc_output to the decoder for the attention mechanism
        predictions, dec_hidden, _ = decoder(dec_input, dec_hidden, enc_output)
        loss += loss_function(targ[:, t], predictions)
        # Find the most likely predicted word from entire vocab at step t (for entire batch)
        predicted_id = tf.argmax(predictions, axis=1)
        # Predicted ID is fed back into the model
        dec_input = tf.expand_dims(predicted_id, 1)
        
    # Average loss over each prediction in output sequence 
    batch_loss = (loss / int(targ.shape[1]))

    return batch_loss


# ------------------------------------------------------------------------
# Evaluation and Testing functions
# ------------------------------------------------------------------------

def evaluate(sentence):
    '''Process unicode string sentence as input to network'''
    attention_plot = np.zeros((max_length_targ, max_length_inp))

    # Clean the sentence (remove some characters and split the punctuation)
    sentence = preprocess_sentence(sentence)

    # Split the sentence up into words and then map each word to its word-id
    inputs = [inp_lang.word_index[word] for word in sentence.split(" ")]

    # Pad the list to maximum input length
    inputs = tf.keras.preprocessing.sequence.pad_sequences(
        [inputs], maxlen=max_length_inp, padding="post"
    )
    inputs = tf.convert_to_tensor(inputs)

    result = ""

    hidden = [tf.zeros((1, units))]

    # Pass input to encoder
    enc_out, enc_hidden = encoder(inputs, hidden)

    # Initialise decoder hidden state with the encoder's and start
    dec_hidden = enc_hidden

    # Add a batch dimension to a single element
    dec_input = tf.expand_dims([targ_lang.word_index["<start>"]], 0)

    for t in range(max_length_targ):
        predictions, dec_hidden, attention_weights = decoder(
            dec_input, dec_hidden, enc_out
        )

        # storing the attention weights to plot later on
        attention_weights = tf.reshape(attention_weights, (-1,))
        attention_plot[t] = attention_weights.numpy()

        # Grab the most likely prediction
        predicted_id = tf.argmax(predictions[0]).numpy()

        # Convert id to word and add to the result sentence
        result += targ_lang.index_word[predicted_id] + " "

        if targ_lang.index_word[predicted_id] == "<end>":
            return result, sentence, attention_plot

        # the predicted ID is fed back into the model
        dec_input = tf.expand_dims([predicted_id], 0)

    return result, sentence, attention_plot

def plot_attention(attention, sentence, predicted_sentence):
    '''Plot attention weights'''
    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_subplot(1, 1, 1)
    ax.matshow(attention, cmap="viridis")

    fontdict = {"fontsize": 14}

    ax.set_xticklabels([""] + sentence, fontdict=fontdict, rotation=90)
    ax.set_yticklabels([""] + predicted_sentence, fontdict=fontdict)

    ax.xaxis.set_major_locator(ticker.MultipleLocator(1))
    ax.yaxis.set_major_locator(ticker.MultipleLocator(1))

    plt.savefig('./output_plots/attention_'+''.join(sentence[1:-2]) + '.png')
    # plt.show()

def translate(sentence):
    '''Uses trained network to translate input sentence and plots attention weights'''
    result, sentence, attention_plot = evaluate(sentence)

    print("Input: %s" % (sentence))
    print("Predicted translation: {}".format(result))

    attention_plot = attention_plot[
        : len(result.split(" ")), : len(sentence.split(" "))
    ]
    plot_attention(attention_plot, sentence.split(" "), result.split(" "))

def getValidationPair(x):
    '''Returns sentence pair as lists from validation set as index x (input, target) '''
    inp_sentence = []
    for index in input_tensor_val[x]:
        if index > 2:
            inp_sentence.append(inp_lang.index_word[index])

    targ_sentence = []
    for index in target_tensor_val[x]:
        if index > 2:
            targ_sentence.append(targ_lang.index_word[index])

    return inp_sentence, targ_sentence

def validate(key):
    inp, targ = getValidationPair(key)

    print('Ground truth sentence: {}'.format(" ".join(targ)))
    result, sentence, attention_plot = evaluate(" ".join(inp))
    print('Input: %s' % (sentence))
    print('Predicted translation: {}'.format(result))
    print('Ground truth: {}'.format(targ))
    print('Predicted translation: {}'.format(result.split(' ')[:-2]))

def gt_vs_prediction(key):
    '''Returns a pair of tuples - the ground truth and predicted sentence'''
    inp, targ = getValidationPair(key)
    result, sentence, attention_plot = evaluate(" ".join(inp))
    return targ, result.split(' ')[:-2]

def estimate_corpus_bleu():
    '''Estimates BLUE score for the translations using up to 1000 samples from validation set'''
    list_references = []
    list_hypotheses = []

    range_end = len(input_tensor_val) if (
        len(input_tensor_val) < 1000) else 1000

    for i in range(0, range_end):
        ground_truth, prediction = gt_vs_prediction(i)
        list_references.append([ground_truth])
        list_hypotheses.append(prediction)

    # for i, prediction in enumerate(list_hypotheses):
    #   print(list_references[i], '\t', prediction)

    return corpus_bleu(list_references, list_hypotheses)

# ------------------------------------------------------------------------
# Dataset extraction and formatting
# ------------------------------------------------------------------------


path_to_file = './eng-ger.txt'
num_examples = 150000

# Load dataset into vectorised tensors
print("* Loading {} datapoints from {}...\n".format(num_examples, path_to_file))
logging.info("* Loading {} datapoints from {}...\n".format(num_examples, path_to_file))

input_tensor, target_tensor, inp_lang, targ_lang = load_dataset(
    path_to_file, num_examples
)

# Calculate max_length of the target tensors before splitting (so model works for both)
max_length_targ, max_length_inp = max_length(
    target_tensor), max_length(input_tensor)

# Creating training and validation sets using an 90-10 split
input_tensor_train, input_tensor_val, target_tensor_train, target_tensor_val = train_test_split(
    input_tensor, target_tensor, test_size=0.1
)

# Find size of vocabulary
vocab_inp_size = len(inp_lang.word_index) + 1
vocab_tar_size = len(targ_lang.word_index) + 1

# Check what the data looks like
print("* Dataset loaded...\n")
logging.info("* Dataset loaded...\n")
print("Input Train, \t Target Train, \t Input Val,\t Target Val")
print(
    len(input_tensor_train), "\t\t",
    len(target_tensor_train), "\t\t",
    len(input_tensor_val), "\t\t",
    len(target_tensor_val)
)
print("\nInput Language, max sentence length: \t{} ".format(max_length_inp))
print("Target Language, max sentence length: \t{}\n".format(max_length_targ))
print("Size of input vocabulary: \t{}".format(vocab_inp_size))
logging.info("Size of input vocabulary: \t{}".format(vocab_inp_size))
print("Size of target vocabulary: \t{}\n".format(vocab_tar_size))
logging.info("Size of target vocabulary: \t{}\n".format(vocab_tar_size))

# Experiment with converting returned tensor to words from the language
# Note: inp_lang and targ_lang are tokenisers
print("Testing Input Language index to word mapping")
convert(inp_lang, input_tensor_train[10])
print()
print("Testing Target Language index to word mapping")
convert(targ_lang, target_tensor_train[10])

# ------------------------------------------------------------------------
# Tuning Parameters
# ------------------------------------------------------------------------

# Size of the dataset
BUFFER_SIZE = len(input_tensor_train)
# Batch size
BATCH_SIZE = 64
# Number of batches per epoch
steps_per_epoch = len(input_tensor_train) // BATCH_SIZE
number_val_batches  = len(input_tensor_val)//BATCH_SIZE
# Embedding dimension for word embeddings (first layer in encoder and decoder model)
embedding_dim = 256
# Dimension of the hidden state (and output space) of encoder and decoder
units = 1024
# Optimiser
optimizer = tf.keras.optimizers.Adam()
# Loss function
loss_object = tf.keras.losses.SparseCategoricalCrossentropy(
    from_logits=True, reduction="none"
)


# ------------------------------------------------------------------------
# Tensorflow Dataset creation
# ------------------------------------------------------------------------

dataset = tf.data.Dataset.from_tensor_slices(
    (input_tensor_train, target_tensor_train)
).shuffle(BUFFER_SIZE)
dataset = dataset.batch(BATCH_SIZE, drop_remainder=True)

validation_dataset = tf.data.Dataset.from_tensor_slices(
    (input_tensor_val, target_tensor_val)).shuffle(BUFFER_SIZE)
validation_dataset = validation_dataset.batch(BATCH_SIZE, drop_remainder=True)

# ------------------------------------------------------------------------
# Initialise the ENCODER and test
# ------------------------------------------------------------------------
# Extract a sample from the database
# Note that the size is (batch_size, sequence_length)
encoder = Encoder(vocab_inp_size, embedding_dim, units, BATCH_SIZE)

example_input_batch, example_target_batch = next(iter(dataset))
print(example_input_batch.shape, example_target_batch.shape)

# Note that the size of the output has 11 dimensions. This is becuase the output at each timestep is being returned (return_sequence = True)
sample_hidden = encoder.initialize_hidden_state()
sample_output, sample_hidden = encoder(example_input_batch, sample_hidden)

print(
    "\n\nTesting:\nEncoder output shape: (batch size, sequence length, units) {}".format(
        sample_output.shape
    )
)
print("Encoder Hidden state shape: (batch size, units) {}".format(sample_hidden.shape))

# ------------------------------------------------------------------------
# Test Attention layer
# ------------------------------------------------------------------------
attention_layer = BahdanauAttention(10)
context_vector, attention_weights = attention_layer(
    sample_hidden, sample_output)

print("\nContext vector shape: (batch size, units) {}".format(context_vector.shape))
print(
    "Attention weights shape: (batch_size, sequence_length, 1) {}\n\n".format(
        attention_weights.shape
    )
)

# ------------------------------------------------------------------------
# Experiment with the decoder
# ------------------------------------------------------------------------
decoder = Decoder(vocab_tar_size, embedding_dim, units, BATCH_SIZE)

sample_decoder_output, _, _ = decoder(
    tf.random.uniform((64, 1)), sample_hidden, sample_output
)

# Decoder output is the size of the target language vocabulary for each of the
# inputs it is given (in this case a batch of 64)

print(
    "\nDecoder output shape: (batch_size, vocab size) {}\n\n".format(
        sample_decoder_output.shape
    )
)

# ------------------------------------------------------------------------
# Set up Checkpoints
# ------------------------------------------------------------------------
checkpoint_dir = "./training_checkpoints"
checkpoint_prefix = os.path.join(checkpoint_dir, "ckpt")
checkpoint = tf.train.Checkpoint(
    optimizer=optimizer, encoder=encoder, decoder=decoder)

# ------------------------------------------------------------------------
# Start TRAINING
# ------------------------------------------------------------------------
print("\nStarting the training...")
logging.info("\nStarting the training...")

#checkpoint.restore(tf.train.latest_checkpoint(checkpoint_dir))

EPOCHS = 50
batch_losses = []
training_losses = []
validation_losses = []

'''Training Loop'''
for epoch in range(EPOCHS):
    start = time.time()

    enc_hidden = encoder.initialize_hidden_state()
    total_loss = 0
    total_val_loss = 0

    # Process Training Data
    for (batch, (inp, targ)) in enumerate(dataset.take(steps_per_epoch)):
        # batch: a counting variable for the batch number
        # inp:   the set of [BATCH SIZE] vectorised input sentences
        # targ:  the set of [BATCH SIZE] vectorised target sentences

        batch_loss = train_step(inp, targ, enc_hidden)
        batch_losses.append(batch_loss.numpy())
        total_loss += batch_loss

        if batch % 100 == 0:
            print('Epoch {} Batch {}/{} Loss {:.4f}'.format(epoch + 1,batch,steps_per_epoch,batch_loss.numpy()))
            logging.info('Epoch {} Batch {}/{} Loss {:.4f}'.format(epoch + 1,batch,steps_per_epoch,batch_loss.numpy()))

    print("----------------------------------------")
    print('Training time: {:.4f} sec'.format(time.time() - start))

    # Validation Losses each epoch
    for (batch, (inp, targ)) in enumerate(validation_dataset.take(number_val_batches)):
        val_batch_loss = validation_step(inp, targ, enc_hidden)
        total_val_loss += val_batch_loss

        if batch % 100 == 0:
            print('Validation batch loss {:.4f}'.format(val_batch_loss.numpy()))

    total_val_loss = (total_val_loss/number_val_batches).numpy()
    validation_losses.append(total_val_loss)

    # Test model accuracy on the validation data
    bleu_score = estimate_corpus_bleu()

    # Saving (checkpoint) the model every 2 epochs
    if (epoch + 1) % 2 == 0:
        checkpoint.save(file_prefix=checkpoint_prefix)

    epoch_loss = total_loss / steps_per_epoch
    training_losses.append(epoch_loss)

    print('Epoch {}: \n\t- Average Epoch Loss: {:.4f} \n\t- Estimate BLEU Score: {:.4f}'.format(
        epoch + 1,
        epoch_loss,
        bleu_score))
    print('\t- Validation Loss: {:.4f}'.format(total_val_loss))
    print('\t- Time taken: {:.4f} sec'.format(time.time() - start))
    print("----------------------------------------")

    logging.info('Epoch {}: \n\t- Average Epoch Loss: {:.4f} \n\t- Estimate BLEU Score: {:.4f}'.format(
        epoch + 1,
        epoch_loss,
        bleu_score))
    logging.info('\t- Validation Loss: {:.4f}'.format(total_val_loss))
    logging.info('\t- Time taken: {:.4f} sec'.format(time.time() - start))
    logging.info("----------------------------------------")

    # Write out batch losses to a CSV file
    with open('50_epochs_150000_sentences.losses', 'w') as file:
        csv_writer = csv.writer(file, delimiter=',',
                            quotechar='"', quoting=csv.QUOTE_MINIMAL)
        csv_writer.writerow(batch_losses)
        csv_writer.writerow(validation_losses)
        csv_writer.writerow(training_losses)


print("\nTraining is complete...\n\n")
logging.info("\nTraining is complete...\n\n")

# ------------------------------------------------------------------------
# Save results from testing
# ------------------------------------------------------------------------


# Plot and save the loss information
plt.figure()
plt.plot(batch_losses)
plt.xlabel('Batches')
plt.ylabel('Training Losses')
plt.savefig('./output_plots/training_losses.png')

plt.figure()
plt.plot(validation_losses)
plt.xlabel('Epochs')
plt.ylabel('Validation Losses')
plt.savefig('./output_plots/validation_losses.png')

# ------------------------------------------------------------------------
# Start TESTING
# ------------------------------------------------------------------------
print("\nStarting the testing...")
logging.info("\nStarting the testing...")

# Restoring the latest checkpoint
checkpoint.restore(tf.train.latest_checkpoint(checkpoint_dir))

# Sentences from validation set
validate(50)
validate(1000)
validate(250)

# Random new sentences
translate("I am hungry")
translate("when will you arrive?")
translate("what do you want for breakfast?")
translate("I like it in boston.")
