# Neural Machine Translation 

![](./gif/mcdoogle.gif)

### Conda Setup
If you would like to use this repository, I have provided an environement `environment.yml` file to be used with Anaconda. This should provide all of the dependencies required to run the GUI, or train the models yourself.

### Checkpoints
Each one of the checkpoints is close to 500 MB. If you require a pre-trained model, please message me to request these files. 

### Run Mini Application
There are a few command line options that you can look at. But, from __attention/eng-xxx/__ run:

```python
python nmt.py --lang ge3 --gui 
```
